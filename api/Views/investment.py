from cv2 import exp
from flask import Blueprint,g, jsonify, session
from flask_expects_json import expects_json

from .. import db
from ..models import Investment, Portfolio, User
from datetime import datetime


BASE_URI = '/investments'
investment = Blueprint('investment', __name__)

create_schema = {
    'type':'object',
    'properties':{
        'name' : {'type':'string'},
        'short_name_handle' : {'type':'string'},
        'date' : {'type': 'string'},
        'price_per_unit' : {'type': 'string'},
        'amount' : {'type':'string'},
        'value' : {'type':'string'},
    },
    'required':[]
}

@investment.route(BASE_URI, methods = ['POST'])
@expects_json(create_schema)
def create_investment():
    user = User.query.filter_by(id = session['user_id']).first()
    if not user.portfolio:
        print("Createad portfolio.")
        user.portfolio = Portfolio()
    
    inv1 = Investment(
        name = g.data['name'],
        short_name_handle = g.data['short_name_handle'],
        date = datetime.fromisoformat(g.data['date']),
        price_per_unit = float(g.data['price_per_unit']),
        amount = float(g.data['amount']),
        value = float(g.data['price_per_unit']) * float(g.data['amount'])
    )
    user.portfolio.investments.append(inv1)
    db.session.commit()

    return jsonify(inv1.serialize())

@investment.route(BASE_URI+"/<id>", methods = ['GET'])
def read(id):
    inv = Investment.query.filter_by(id = id, deleted = False).first()
    if inv:
        return jsonify(inv.serialize())
    else:
        return "Objected not found."

@investment.route(BASE_URI+"/<id>", methods = ['PUT'])
@expects_json(create_schema)
def update(id):
    old_inv = Investment.query.filter_by(id = id).first()
    if not old_inv:
        return "Object not found."

    old_inv.name = g.data['name']
    old_inv.short_name_handle = g.data['short_name_handle']
    old_inv.date = datetime.fromisoformat(g.data['date'])
    old_inv.price_per_unit = float(g.data['price_per_unit'])
    old_inv.amount = float(g.data['amount'])
    old_inv.value = float(g.data['price_per_unit']) * float(g.data['amount'])

    db.session.commit()
        
    return jsonify(old_inv.serialize())

@investment.route(BASE_URI+"/<id>", methods = ['DELETE'])
def deleted(id):
    inv = Investment.query.filter_by(id=id).first()
    inv.deleted = True
    db.session.commit()

    return f"Deleted id={id}"