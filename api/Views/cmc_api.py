from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json

def fetch_data(shorts):
    url = 'https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
    parameters = {
        'symbol' : shorts
        }
    headers = {
        'Accepts': 'application/json',
        'X-CMC_PRO_API_KEY': '550be28e-8ac7-4d7f-8845-2ea63c064198',
        }

    session = Session()
    session.headers.update(headers)

    try:
        values = dict()
        response = session.get(url, params=parameters)
        data = json.loads(response.text)
        for s in shorts:
            values[s] = data['data'][s]['quote']['USD']['price']
        return values
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
        return None