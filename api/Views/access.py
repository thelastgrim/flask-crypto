from flask import Blueprint, request
import bcrypt
from flask_login import login_user
from flask import session, Response
from ..models import User
from .. import db, scheduler

access = Blueprint('access', __name__)


@access.route("/sch/<seconds>", methods = ['PUT'])
def change_intervar(seconds):
    print(f'Job rescheduled - interval = {seconds} seconds.')
    scheduler.get_job(id='sch_1').reschedule(trigger = 'interval', seconds = int(seconds))
    return seconds


@access.route("/login", methods =['POST'])
def login():
    email = request.form.get('email')
    password = request.form.get('password') #.encode('utf8')
    
    user = User.query.filter_by(email=email).first()
    if user:
        if bcrypt.checkpw(password.encode('utf-8'),user.password.encode('utf-8')):
            login_user(user, remember=True)
            session['user_id'] = user.id
            return Response(status=200, mimetype='application/json')
        else:
            return Response('Username or password incorrect', status=400, mimetype='application/json')
    else:
        return Response('Username or password incorrect', status=400, mimetype='application/json')

@access.route("/register", methods=['POST'])
def register():
    email = request.form.get('email')
    password = request.form.get('password')

    password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8') #mora decode jer postgres driver
                                                                                         #encoduje vec encodovano
    new_user = User(email=email, password=password)
    if (add_user(new_user)):
        return Response ("Created.", status=201, mimetype='application/json')
    else:
        return Response ("Email already exists.", status=400, mimetype='application/json')

   
def add_user(user):
    new_user = User.query.filter_by(email=user.email).first()
    if (new_user):
        return None
    
    db.session.add(user)
    db.session.commit()

    return user

