from flask import Flask, session
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
from flask_login import LoginManager
from flask_apscheduler import APScheduler as _BaseAPScheduler
from .Views.cmc_api import fetch_data



class APScheduler(_BaseAPScheduler):
    def run_job(self, id, jobstore=None):
        with self.app.app_context():
            super().run_job(id=id, jobstore=jobstore)

db = SQLAlchemy()
scheduler = APScheduler()

DB_NAME = "database.db"

app = Flask(__name__)

def schedule():
    from .models import Portfolio
    shorts = set()
    with app.app_context():
        #a = db.session.query(User).filter_by(id=1).first()
        portfolios = db.session.query(Portfolio).all()
        for p in portfolios:
            for i in p.investments:
                if not i.deleted:
                    shorts.add(i.short_name_handle)
        print('do something')
        
    data = fetch_data(list(shorts))

    with app.app_context():
        portfolios = db.session.query(Portfolio).all()
        for p in portfolios:
            total = 0
            for i in p.investments:
                if not i.deleted:
                    total += i.value*data[i.short_name_handle]
            print(total)
            p.value = total
        print('do something')
        db.session.commit()
def create_app():
    
    app.config['SECRET_KEY'] = 'secret'
    app.config['SQLALCHEMY_DATABASE_URI']=f'sqlite:///{DB_NAME}'
    db.init_app(app)

    app.config['SCHEDULER_API_ENABLED'] = True
    app.config['JOBS'] = [
        {
            'id': 'sch_1',
            'func': schedule,
            'trigger': 'interval',
            'seconds': 3600,
            'replace_existing': True
        }
    ]
 

    
    from .Views.investment import investment
    from .Views.access import access

    app.register_blueprint(investment, url_prefix='/')
    app.register_blueprint(access, url_prefix = '/')

    with app.app_context():
        db.create_all()

    login_manager = LoginManager()
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))


    scheduler.init_app(app)
    scheduler.start() 

    return app


