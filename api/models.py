
from . import db
from datetime import datetime
from .Helper.Serializer import Serializer
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKey
from flask_login import UserMixin


class User(db.Model, UserMixin):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(100), unique = True)
    password = db.Column(db.String(150))

    portfolio = relationship("Portfolio", back_populates="user", uselist=False)
  


class Portfolio(db.Model):
    __tablename__ = "portfolio"
    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates='portfolio')
   
    #OneToMany
    investments = relationship("Investment")

    value = db.Column(db.Float)

class Investment(db.Model):
    __tablename__ = 'investment'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    short_name_handle = db.Column(db.String(10))
    date = db.Column(db.DateTime, default = datetime.utcnow)
    price_per_unit = db.Column(db.Float)
    amount = db.Column(db.Float)
    value = db.Column(db.Float)
    deleted = db.Column(db.Boolean, default= False)

    portfolio_id = db.Column(db.Integer, ForeignKey('portfolio.id'))


    def serialize(self):
        d = Serializer.serialize(self)
        return d
